# Translation for kcm_pulseaudio.po to Euskara/Basque (eu).
# Copyright (C) 2018-2022, This file is copyright:
# This file is distributed under the same license as the plasma-pa package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2017, 2018, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-pa\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-23 01:00+0000\n"
"PO-Revision-Date: 2022-11-03 05:45+0100\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.1\n"

#: context.cpp:596
#, kde-format
msgctxt "Name shown in debug pulseaudio tools"
msgid "Plasma PA"
msgstr "Plasma PA"

#: kcm/package/contents/ui/CardListItem.qml:52
#: kcm/package/contents/ui/DeviceListItem.qml:127
#, kde-format
msgctxt "@label"
msgid "Profile:"
msgstr "Profila:"

#: kcm/package/contents/ui/DeviceListItem.qml:47
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: kcm/package/contents/ui/DeviceListItem.qml:88
#, kde-format
msgid "Port:"
msgstr "Ataka:"

#: kcm/package/contents/ui/DeviceListItem.qml:111 qml/listitemmenu.cpp:387
#, kde-format
msgctxt "Port is unavailable"
msgid "%1 (unavailable)"
msgstr "%1 (eskuraezina)"

#: kcm/package/contents/ui/DeviceListItem.qml:113 qml/listitemmenu.cpp:389
#, kde-format
msgctxt "Port is unplugged"
msgid "%1 (unplugged)"
msgstr "%1 (entxufatu gabe)"

#: kcm/package/contents/ui/DeviceListItem.qml:180
#, kde-format
msgctxt "Placeholder is channel name"
msgid "%1:"
msgstr "%1:"

#: kcm/package/contents/ui/DeviceListItem.qml:213
#, kde-format
msgctxt "Perform an audio test of the device"
msgid "Test"
msgstr "Proba"

#: kcm/package/contents/ui/DeviceListItem.qml:222
#, kde-format
msgctxt "Audio balance (e.g. control left/right volume individually"
msgid "Balance"
msgstr "Oreka"

#: kcm/package/contents/ui/main.qml:24
#, kde-format
msgid "This module allows configuring the Pulseaudio sound subsystem."
msgstr "Modulu honek PulseAudio soinu azpisistema konfiguratzen uzten du."

#: kcm/package/contents/ui/main.qml:83
#, kde-format
msgid "Show Inactive Devices"
msgstr "Erakutsi aktibo ez dauden gailuak"

#: kcm/package/contents/ui/main.qml:92
#, kde-format
msgid "Configure Volume Controls…"
msgstr "Konfiguratu bolumen-aginteak…"

#: kcm/package/contents/ui/main.qml:99
#, kde-format
msgid "Configure…"
msgstr "Konfiguratu…"

#: kcm/package/contents/ui/main.qml:102
#, kde-format
msgid "Requires %1 PulseAudio module"
msgstr "%1 PulseAudio modulua behar du"

#: kcm/package/contents/ui/main.qml:105
#, kde-format
msgid ""
"Add virtual output device for simultaneous output on all local sound cards"
msgstr ""
"Gehitu alegiazko irteera gailua, irteera aldi berean soinu txartel lokal "
"guztietara bideratzeko"

#: kcm/package/contents/ui/main.qml:111
#, kde-format
msgid ""
"Automatically switch all running streams when a new output becomes available"
msgstr ""
"Automatikoki aldatu martxan dauden korronte guztiak irteerako berri bat "
"erabilgarri jartzen denean"

#: kcm/package/contents/ui/main.qml:148
#, kde-format
msgid "Playback Devices"
msgstr "Atzera-jotzeko gailuak"

#: kcm/package/contents/ui/main.qml:172
#, kde-format
msgid "Recording Devices"
msgstr "Grabatzeko gailuak"

#: kcm/package/contents/ui/main.qml:196
#, kde-format
msgid "Inactive Cards"
msgstr "Txartel ez aktiboak"

#: kcm/package/contents/ui/main.qml:230
#, kde-format
msgid "Playback Streams"
msgstr "Atzera-jotze korronteak"

#: kcm/package/contents/ui/main.qml:279
#, kde-format
msgid "Recording Streams"
msgstr "Grabatze korronteak"

#: kcm/package/contents/ui/main.qml:313
#, kde-format
msgctxt ""
"%1 is an error string produced by an external component, and probably "
"untranslated"
msgid ""
"Error trying to play a test sound. \n"
"The system said: \"%1\""
msgstr ""
"Errorea probako soinu bat jotzeko saiakeran.\n"
"Sistemak esan du: \"%1\""

#: kcm/package/contents/ui/main.qml:348
#, kde-format
msgid "Front Left"
msgstr "Aurreko ezkerrekoa"

#: kcm/package/contents/ui/main.qml:349
#, kde-format
msgid "Front Center"
msgstr "Aurreko erdikoa"

#: kcm/package/contents/ui/main.qml:350
#, kde-format
msgid "Front Right"
msgstr "Aurreko eskuinekoa"

#: kcm/package/contents/ui/main.qml:351
#, kde-format
msgid "Side Left"
msgstr "Alboko ezkerrekoa"

#: kcm/package/contents/ui/main.qml:352
#, kde-format
msgid "Side Right"
msgstr "Alboko eskuinekoa"

#: kcm/package/contents/ui/main.qml:353
#, kde-format
msgid "Rear Left"
msgstr "Atzeko ezkerrekoa"

#: kcm/package/contents/ui/main.qml:354
#, kde-format
msgid "Subwoofer"
msgstr "Azpibaxuen bozgorailua"

#: kcm/package/contents/ui/main.qml:355
#, kde-format
msgid "Rear Right"
msgstr "Atzeko eskuinekoa"

#: kcm/package/contents/ui/main.qml:356
#, kde-format
msgid "Mono"
msgstr "Mono"

#: kcm/package/contents/ui/main.qml:466
#, kde-format
msgid "Click on any speaker to test sound"
msgstr "Egin klik bozgorailuetako edozeinetan soinua probatzeko"

#: kcm/package/contents/ui/MuteButton.qml:22
#, kde-format
msgctxt "Unmute audio stream"
msgid "Unmute %1"
msgstr "Ozendu %1"

#: kcm/package/contents/ui/MuteButton.qml:22
#, kde-format
msgctxt "Mute audio stream"
msgid "Mute %1"
msgstr "Isilarazi %1"

#: kcm/package/contents/ui/StreamListItem.qml:57
#, kde-format
msgid "Notification Sounds"
msgstr "Jakinarazpen soinuak"

#: kcm/package/contents/ui/StreamListItem.qml:63
#, kde-format
msgctxt "label of stream items"
msgid "%1: %2"
msgstr "%1: %2"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:15
#, kde-format
msgid "Volume Controls"
msgstr "Bolumen-aginteak"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:31
#, kde-format
msgid "Raise maximum volume"
msgstr "Altxatu bolumen muga"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:39
#, kde-format
msgid "Volume change step:"
msgstr "Bolumen aldaketa urratsa:"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:63
#, kde-format
msgid "Play audio feedback for changes to:"
msgstr "Jo audio-berrelikatzea hau aldatzen denean:"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:64
#: kcm/package/contents/ui/VolumeControlsConfig.qml:73
#, kde-format
msgid "Audio volume"
msgstr "Audioaren bolumena"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:72
#, kde-format
msgid "Show visual feedback for changes to:"
msgstr "Erakutsi ikus-berrelikatzea hau aldatzen denean:"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:80
#, kde-format
msgid "Microphone sensitivity"
msgstr "Mikrofonoaren sentikortasuna"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:87
#, kde-format
msgid "Mute state"
msgstr "Isilarazte egoera"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:94
#, kde-format
msgid "Default output device"
msgstr "Irteerako gailu lehenetsia"

#: kcm/package/contents/ui/VolumeSlider.qml:51
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%%1"

#: kcm/package/contents/ui/VolumeSlider.qml:71
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "% 100"

#: qml/listitemmenu.cpp:342
#, kde-format
msgid "Play all audio via this device"
msgstr "Jo audio guztia gailu honen bidez"

#: qml/listitemmenu.cpp:347
#, kde-format
msgid "Record all audio via this device"
msgstr "Grabatu audio guztia gailu honen bidez"

#: qml/listitemmenu.cpp:375
#, kde-format
msgctxt ""
"Heading for a list of ports of a device (for example built-in laptop "
"speakers or a plug for headphones)"
msgid "Ports"
msgstr "Atakak"

#: qml/listitemmenu.cpp:445
#, kde-format
msgctxt ""
"Heading for a list of device profiles (5.1 surround sound, stereo, speakers "
"only, ...)"
msgid "Profiles"
msgstr "Profilak"

#: qml/listitemmenu.cpp:479
#, kde-format
msgctxt ""
"Heading for a list of possible output devices (speakers, headphones, ...) to "
"choose"
msgid "Play audio using"
msgstr "Audioa jo hau erabiliz"

#: qml/listitemmenu.cpp:481
#, kde-format
msgctxt ""
"Heading for a list of possible input devices (built-in microphone, "
"headset, ...) to choose"
msgid "Record audio using"
msgstr "Audioa grabatu hau erabiliz"

#: qml/microphoneindicator.cpp:102
#, kde-format
msgid "Mute"
msgstr "Isilarazi"

#: qml/microphoneindicator.cpp:132 qml/microphoneindicator.cpp:134
#, kde-format
msgid "Microphone"
msgstr "Mikrofonoa"

#: qml/microphoneindicator.cpp:134
#, kde-format
msgid "Microphone Muted"
msgstr "Mikrofonoa isilarazita"

#: qml/microphoneindicator.cpp:284
#, kde-format
msgctxt "list separator"
msgid ", "
msgstr ", "

#: qml/microphoneindicator.cpp:284
#, kde-format
msgctxt "List of apps is using mic"
msgid "%1 are using the microphone"
msgstr "%1 mikrofonoa erabiltzen ari dira"

#: qml/microphoneindicator.cpp:310
#, kde-format
msgctxt "App %1 is using mic with name %2"
msgid "%1 is using the microphone (%2)"
msgstr "%1 mikrofonoa erabiltzen ari da (%2)"

#: qml/microphoneindicator.cpp:317
#, kde-format
msgctxt "App is using mic"
msgid "%1 is using the microphone"
msgstr "%1 mikrofonoa erabiltzen ari da"

#~ msgid "100%"
#~ msgstr "% 100"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Iñigo Salvador Azurmendi"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "xalba@ni.eus"

#~ msgctxt "@title"
#~ msgid "Audio"
#~ msgstr "Audioa"

#~ msgctxt "@info:credit"
#~ msgid "Copyright 2015 Harald Sitter"
#~ msgstr "Copyright 2015 Harald Sitter"

#~ msgctxt "@info:credit"
#~ msgid "Harald Sitter"
#~ msgstr "Harald Sitter"

#~ msgctxt "@info:credit"
#~ msgid "Author"
#~ msgstr "Egilea"

#~ msgid "Configure"
#~ msgstr "Konfiguratu"

#~ msgid "Device Profiles"
#~ msgstr "Gailuen profilak"

#~ msgid "Advanced Output Configuration"
#~ msgstr "Irteera konfigurazio aurreratua"

#~ msgid "Speaker Placement and Testing"
#~ msgstr "Bozgorailuak kokatzea eta probatzea"

#~ msgctxt "@label"
#~ msgid "Output:"
#~ msgstr "Irteera:"

#~ msgctxt "Port is unavailable"
#~ msgid " (unavailable)"
#~ msgstr " (eskuraezina)"

#~ msgctxt "Port is unplugged"
#~ msgid " (unplugged)"
#~ msgstr " (entxufatu gabe)"

#~ msgid "Configure..."
#~ msgstr "Konfiguratu..."

#~ msgctxt "@label"
#~ msgid "No Device Profiles Available"
#~ msgstr "Ez dago gailu-profil erabilgarririk"

#~ msgctxt "@label"
#~ msgid "No Playback Devices Available"
#~ msgstr "Ez dago atzera-jotzeko gailu erabilgarririk"

#~ msgctxt "@label"
#~ msgid "No Recording Devices Available"
#~ msgstr "Ez dago grabatzeko gailu erabilgarririk"

#~ msgctxt "@label"
#~ msgid "No Applications Playing Audio"
#~ msgstr "Ez dago audioa jotzen ari den aplikaziorik"

#~ msgctxt "@label"
#~ msgid "No Applications Recording Audio"
#~ msgstr "Ez dago soinua grabatzen ari den aplikaziorik"

#~ msgctxt "@title:tab"
#~ msgid "Devices"
#~ msgstr "Gailuak"

#~ msgctxt "@title:tab"
#~ msgid "Applications"
#~ msgstr "Aplikazioak"

#~ msgctxt "@title:tab"
#~ msgid "Advanced"
#~ msgstr "Aurreratua"

#~ msgid "Outputs"
#~ msgstr "Irteerak"

#~ msgctxt "@label"
#~ msgid "No Output Devices Available"
#~ msgstr "Ez dago irteera gailu erabilgarririk"

#~ msgid "Inputs"
#~ msgstr "Sarrerak"

#~ msgctxt "@title"
#~ msgid "Configure the Audio Volume"
#~ msgstr "Konfiguratu audioaren bolumena"

#~ msgid "Capture"
#~ msgstr "Atzeman"
