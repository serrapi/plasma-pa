# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-pa package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-pa\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-31 01:00+0000\n"
"PO-Revision-Date: 2023-02-01 02:19+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#: contents/ui/DeviceListItem.qml:26
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: contents/ui/DeviceListItem.qml:35
#, kde-format
msgid "Device name not found"
msgstr "მოწყობილობა ნაპოვნი არაა"

#: contents/ui/ListItemBase.qml:70
#, kde-format
msgid "Currently not recording"
msgstr "ამჟამად ჩაწერა არ მიმდინარეობს"

#: contents/ui/ListItemBase.qml:71
#, kde-format
msgid "Currently not playing"
msgstr "ამჟამად დაკვრა არ მიმდინარეობს"

#: contents/ui/ListItemBase.qml:184
#, kde-format
msgctxt "@action:button"
msgid "Additional Options"
msgstr "დამატებითი პარამეტრები"

#: contents/ui/ListItemBase.qml:186
#, kde-format
msgid "Show additional options for %1"
msgstr "%1-სთვის დამატებითი პარამეტრების ჩვენება"

#: contents/ui/ListItemBase.qml:203
#, kde-format
msgctxt "@action:button"
msgid "Unmute"
msgstr "დადუმების გამორთვა"

#: contents/ui/ListItemBase.qml:203
#, kde-format
msgctxt "@action:button"
msgid "Mute"
msgstr "დადუმება"

#: contents/ui/ListItemBase.qml:205
#, kde-format
msgid "Unmute %1"
msgstr "დადუმების გამორთვა: %1"

#: contents/ui/ListItemBase.qml:205
#, kde-format
msgid "Mute %1"
msgstr "დადუმება: %1"

#: contents/ui/ListItemBase.qml:225
#, kde-format
msgctxt "Accessibility data on volume slider"
msgid "Adjust volume for %1"
msgstr "%1-ის ხმის მორგება"

#: contents/ui/ListItemBase.qml:283
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1%"

#: contents/ui/ListItemBase.qml:303
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100%"

#: contents/ui/main.qml:32
#, kde-format
msgid "Audio Volume"
msgstr "აუდიოს ხმა"

#: contents/ui/main.qml:56
#, kde-format
msgid "Audio Muted"
msgstr "აუდიო ჩაჩუმებულია"

#: contents/ui/main.qml:58
#, kde-format
msgid "Volume at %1%"
msgstr "ხმა: %1%"

#: contents/ui/main.qml:75
#, kde-format
msgid "Middle-click to unmute"
msgstr "შუა-წკაპი დადუმების გასაუქმებლად"

#: contents/ui/main.qml:76
#, kde-format
msgid "Middle-click to mute all audio"
msgstr "შუა-წკაპი ყველა ხმის დასადუმებლად"

#: contents/ui/main.qml:77
#, kde-format
msgid "Scroll to adjust volume"
msgstr "ხმის მოსარგებად აქაჩეთ"

#: contents/ui/main.qml:238
#, kde-format
msgid "No output device"
msgstr "გამოტანის მოწყობილობის გარეშე"

#: contents/ui/main.qml:249
#, kde-format
msgctxt "Device name (Battery percent)"
msgid "%1 (%2% Battery)"
msgstr "%1 (%2% ელემენტი)"

#: contents/ui/main.qml:393
#, kde-format
msgid "Increase Volume"
msgstr "ხმის აწევა"

#: contents/ui/main.qml:399
#, kde-format
msgid "Decrease Volume"
msgstr "ხმის დაწევა"

#: contents/ui/main.qml:405
#, kde-format
msgid "Mute"
msgstr "დადუმება"

#: contents/ui/main.qml:411
#, kde-format
msgid "Increase Microphone Volume"
msgstr "მიკროფონის ხმის აწევა"

#: contents/ui/main.qml:417
#, kde-format
msgid "Decrease Microphone Volume"
msgstr "მიკროფონის ხმის დაწევა"

#: contents/ui/main.qml:423
#, kde-format
msgid "Mute Microphone"
msgstr "მიკროფონის დადუმება"

#: contents/ui/main.qml:527
#, kde-format
msgid "Devices"
msgstr "მოწყობილობები"

#: contents/ui/main.qml:534
#, kde-format
msgid "Applications"
msgstr "პროგრამები"

#: contents/ui/main.qml:555 contents/ui/main.qml:557 contents/ui/main.qml:783
#, kde-format
msgid "Force mute all playback devices"
msgstr "ყველა დამკვრელი მოწყობილბის დადუმება"

#: contents/ui/main.qml:591
#, kde-format
msgid "No output or input devices found"
msgstr "არც შესატანი, არც გამოსატანი მოწყობილობა ნაპოვნი არაა"

#: contents/ui/main.qml:610
#, kde-format
msgid "No applications playing or recording audio"
msgstr "ამჟამად აუდიოს არც ერთი აპლიკაცია არ უკრავს და არც იწერს"

#: contents/ui/main.qml:761
#, kde-format
msgid "Raise maximum volume"
msgstr "მაქსიმალური ხმის აწევა"

#: contents/ui/main.qml:787
#, kde-format
msgid "Show virtual devices"
msgstr "ვირტუალური მოწყობილობების ჩვენება"

#: contents/ui/main.qml:793
#, kde-format
msgid "&Configure Audio Devices…"
msgstr "აუდიო მოწყობილობების &მორგება…"

#: contents/ui/StreamListItem.qml:24
#, kde-format
msgid "Stream name not found"
msgstr "ნაკადის სახელი ვერ ვიპოვე"

#~ msgid "General"
#~ msgstr "ძირითადი"

#~ msgid "Volume step:"
#~ msgstr "ხმის ბიჯი:"

#~ msgid "Play audio feedback for changes to:"
#~ msgstr "ხმოვანი შეტყობინება ცვლილებებისთვის:"

#~ msgid "Audio volume"
#~ msgstr "აუდიოს ხმა"

#~ msgid "Show visual feedback for changes to:"
#~ msgstr "ცვლილებისას ვიზუალური ნიშნების მიცემა:"

#~ msgid "Microphone sensitivity"
#~ msgstr "მიკროფონის მგრძნობიარობა"

#~ msgid "Mute state"
#~ msgstr "დადუმების მდგომარეობა"

#~ msgid "Default output device"
#~ msgstr "ნაგულისხმები გამოსატანი მოწყობილობა"

#~ msgctxt "@title"
#~ msgid "Display:"
#~ msgstr "ჩვენება:"
