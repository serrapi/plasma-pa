# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-31 01:00+0000\n"
"PO-Revision-Date: 2023-02-12 08:57+0100\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.2\n"

#: contents/ui/DeviceListItem.qml:26
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: contents/ui/DeviceListItem.qml:35
#, kde-format
msgid "Device name not found"
msgstr "Nie znaleziono nazwy urządzenia"

#: contents/ui/ListItemBase.qml:70
#, kde-format
msgid "Currently not recording"
msgstr "Obecnie nic nie jest nagrywane"

#: contents/ui/ListItemBase.qml:71
#, kde-format
msgid "Currently not playing"
msgstr "Obecnie nic nie jest odtwarzane"

#: contents/ui/ListItemBase.qml:184
#, kde-format
msgctxt "@action:button"
msgid "Additional Options"
msgstr "Dodatkowe ustawienia"

#: contents/ui/ListItemBase.qml:186
#, kde-format
msgid "Show additional options for %1"
msgstr "Pokaż dodatkowe ustawienia dla %1"

#: contents/ui/ListItemBase.qml:203
#, kde-format
msgctxt "@action:button"
msgid "Unmute"
msgstr "Wyłącz wyciszenie"

#: contents/ui/ListItemBase.qml:203
#, kde-format
msgctxt "@action:button"
msgid "Mute"
msgstr "Wycisz"

#: contents/ui/ListItemBase.qml:205
#, kde-format
msgid "Unmute %1"
msgstr "Usuń wyciszenie %1"

#: contents/ui/ListItemBase.qml:205
#, kde-format
msgid "Mute %1"
msgstr "Wycisz %1"

#: contents/ui/ListItemBase.qml:225
#, kde-format
msgctxt "Accessibility data on volume slider"
msgid "Adjust volume for %1"
msgstr "Dostosuj głośność dla %1"

#: contents/ui/ListItemBase.qml:283
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1%"

#: contents/ui/ListItemBase.qml:303
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100%"

#: contents/ui/main.qml:32
#, kde-format
msgid "Audio Volume"
msgstr "Głośność dźwięku"

#: contents/ui/main.qml:56
#, kde-format
msgid "Audio Muted"
msgstr "Dźwięk wyciszony"

#: contents/ui/main.qml:58
#, kde-format
msgid "Volume at %1%"
msgstr "Głośność na %1%"

#: contents/ui/main.qml:75
#, kde-format
msgid "Middle-click to unmute"
msgstr "Środkowy przywraca głośność"

#: contents/ui/main.qml:76
#, kde-format
msgid "Middle-click to mute all audio"
msgstr "Środkowy wycisza dźwięk zupełnie"

#: contents/ui/main.qml:77
#, kde-format
msgid "Scroll to adjust volume"
msgstr "Przewiń, aby dostosować głośność"

#: contents/ui/main.qml:238
#, kde-format
msgid "No output device"
msgstr "Nie ma żadnego urządzenia wyjściowego"

#: contents/ui/main.qml:249
#, kde-format
msgctxt "Device name (Battery percent)"
msgid "%1 (%2% Battery)"
msgstr "%1 (%2% Bateria)"

#: contents/ui/main.qml:393
#, kde-format
msgid "Increase Volume"
msgstr "Zwiększ głośność"

#: contents/ui/main.qml:399
#, kde-format
msgid "Decrease Volume"
msgstr "Zmniejsz głośność"

#: contents/ui/main.qml:405
#, kde-format
msgid "Mute"
msgstr "Wycisz"

#: contents/ui/main.qml:411
#, kde-format
msgid "Increase Microphone Volume"
msgstr "Zwiększ głośność mikrofonu"

#: contents/ui/main.qml:417
#, kde-format
msgid "Decrease Microphone Volume"
msgstr "Zmniejsz głośność mikrofonu"

#: contents/ui/main.qml:423
#, kde-format
msgid "Mute Microphone"
msgstr "Wycisz mikrofon"

#: contents/ui/main.qml:527
#, kde-format
msgid "Devices"
msgstr "Urządzenia"

#: contents/ui/main.qml:534
#, kde-format
msgid "Applications"
msgstr "Programy"

#: contents/ui/main.qml:555 contents/ui/main.qml:557 contents/ui/main.qml:783
#, kde-format
msgid "Force mute all playback devices"
msgstr "Wycisz wszystkie urządzenia odtwarzające"

#: contents/ui/main.qml:591
#, kde-format
msgid "No output or input devices found"
msgstr "Nie znaleziono żadnych urządzeń wejściowych, ani wyjściowych"

#: contents/ui/main.qml:610
#, kde-format
msgid "No applications playing or recording audio"
msgstr "Żadna aplikacja nie odtwarza, ani nie nagrywa teraz dźwięku"

#: contents/ui/main.qml:761
#, kde-format
msgid "Raise maximum volume"
msgstr "Zwiększ graniczną głośność"

#: contents/ui/main.qml:787
#, kde-format
msgid "Show virtual devices"
msgstr "Pokaż urządzenia wirtualne"

#: contents/ui/main.qml:793
#, kde-format
msgid "&Configure Audio Devices…"
msgstr "&Ustawienia urządzeń dźwiękowych..."

#: contents/ui/StreamListItem.qml:24
#, kde-format
msgid "Stream name not found"
msgstr "Nie znaleziono nazwy strumienia"

#~ msgid "General"
#~ msgstr "Ogólne"

#~ msgid "Volume step:"
#~ msgstr "Krok głośności:"

#~ msgid "Play audio feedback for changes to:"
#~ msgstr "Odgrywaj dźwięk zwrotny dla zmian w:"

#~ msgid "Audio volume"
#~ msgstr "Głośność dźwięku"

#~ msgid "Show visual feedback for changes to:"
#~ msgstr "Pokazuj informację zwrotną dla zmian w:"

#~ msgid "Microphone sensitivity"
#~ msgstr "Czułość mikrofonu"

#~ msgid "Mute state"
#~ msgstr "Stan wyciszenia"

#~ msgid "Default output device"
#~ msgstr "Domyślne urządzenie wyjściowe"

#~ msgctxt "@title"
#~ msgid "Display:"
#~ msgstr "Wyświetlacz:"

#~ msgid "Record all audio via this device"
#~ msgstr "Nagrywaj każdy dźwięk tym urządzeniem"

#~ msgid "Play all audio via this device"
#~ msgstr "Odtwarzaj każdy dźwięk tym urządzeniem"

#~ msgctxt ""
#~ "Heading for a list of ports of a device (for example built-in laptop "
#~ "speakers or a plug for headphones)"
#~ msgid "Ports"
#~ msgstr "Porty"

#~ msgctxt "Port is unavailable"
#~ msgid "%1 (unavailable)"
#~ msgstr "%1 (niedostępne)"

#~ msgctxt "Port is unplugged"
#~ msgid "%1 (unplugged)"
#~ msgstr "%1 (niepodłączone)"

#~ msgctxt ""
#~ "Heading for a list of possible output devices (speakers, headphones, ...) "
#~ "to choose"
#~ msgid "Play audio using"
#~ msgstr "Odtwarzaj dźwięk przy użyciu"

#~ msgctxt ""
#~ "Heading for a list of possible input devices (built-in microphone, "
#~ "headset, ...) to choose"
#~ msgid "Record audio using"
#~ msgstr "Nagrywaj dźwięk przy użyciu"

#~ msgid "show hidden devices"
#~ msgstr "pokaż ukryte urządzenia"

#~ msgid "Feedback:"
#~ msgstr "Informacja zwrotna:"

#~ msgid "Play sound when volume changes"
#~ msgstr "Odtwórz dźwięk przy zmianie głośności"

#~ msgid "Display notification when default output device changes"
#~ msgstr "Powiadom przy zmianie urządzenia wyjściowego"

#~ msgid "Maximum volume:"
#~ msgstr "Graniczna głośność:"

#~ msgid "Default Device"
#~ msgstr "Urządzenie domyślne"

#~ msgid "Playback Streams"
#~ msgstr "Odtwarzane strumienie"

#~ msgid "Recording Streams"
#~ msgstr "Nagrywane strumienie"

#~ msgid "Playback Devices"
#~ msgstr "Urządzenia do odtwarzania"

#~ msgid "Recording Devices"
#~ msgstr "Urządzenia do nagrywania"

#~ msgctxt "label of stream items"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgctxt "Checkable switch for (un-)muting sound output."
#~ msgid "Mute"
#~ msgstr "Wycisz"

#~ msgctxt "Checkable switch to change the current default output."
#~ msgid "Default"
#~ msgstr "Domyślnie"

#~ msgid "Capture Streams"
#~ msgstr "Przechwytywane strumienie"

#~ msgid "Capture Devices"
#~ msgstr "Urządzenia nagrywające"

#~ msgid "Volume"
#~ msgstr "Głośność"

#~ msgid "%"
#~ msgstr "%"

#~ msgid "Behavior"
#~ msgstr "Zachowanie"

#~ msgid "Volume feedback"
#~ msgstr "Informacja zwrotna głośności"

#, fuzzy
#~| msgid "Devices"
#~ msgid "Device Profiles"
#~ msgstr "Urządzenia"

#, fuzzy
#~| msgid "Playback Streams"
#~ msgid "Playback"
#~ msgstr "Odtwarzane strumienie"

#, fuzzy
#~| msgid "No applications playing or recording audio"
#~ msgctxt "@label"
#~ msgid "No Applications Playing Audio"
#~ msgstr "Brak aplikacji odtwarzających lub nagrywających dźwięk"

#, fuzzy
#~| msgid "Capture Streams"
#~ msgid "Capture"
#~ msgstr "Przechwytywane strumienie"

#, fuzzy
#~| msgid "No applications playing or recording audio"
#~ msgctxt "@label"
#~ msgid "No Applications Recording Audio"
#~ msgstr "Brak aplikacji odtwarzających lub nagrywających dźwięk"

#, fuzzy
#~| msgctxt ""
#~| "Heading for a list of ports of a device (for example built-in laptop "
#~| "speakers or a plug for headphones)"
#~| msgid "Ports"
#~ msgid "Port"
#~ msgstr "Porty"

#, fuzzy
#~| msgid "Devices"
#~ msgctxt "@title:tab"
#~ msgid "Devices"
#~ msgstr "Urządzenia"

#, fuzzy
#~| msgid "Applications"
#~ msgctxt "@title:tab"
#~ msgid "Applications"
#~ msgstr "Programy"

#~ msgctxt "label of stream items"
#~ msgid "%1: %2"
#~ msgstr "%1: %2"

#, fuzzy
#~| msgctxt "only used for sizing, should be widest possible string"
#~| msgid "100%"
#~ msgid "100%"
#~ msgstr "100%"
