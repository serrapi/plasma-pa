# translation of kcm_pulseaudio.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2015, 2016, 2017, 2019, 2022.
# Mthw <jari_45@hotmail.com>, 2018.
# Matej Mrenica <matejm98mthw@gmail.com>, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcm_pulseaudio\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-23 01:00+0000\n"
"PO-Revision-Date: 2022-10-04 17:50+0200\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.08.1\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: context.cpp:596
#, kde-format
msgctxt "Name shown in debug pulseaudio tools"
msgid "Plasma PA"
msgstr "Plasma PA"

#: kcm/package/contents/ui/CardListItem.qml:52
#: kcm/package/contents/ui/DeviceListItem.qml:127
#, kde-format
msgctxt "@label"
msgid "Profile:"
msgstr "Profil:"

#: kcm/package/contents/ui/DeviceListItem.qml:47
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: kcm/package/contents/ui/DeviceListItem.qml:88
#, kde-format
msgid "Port:"
msgstr "Port:"

#: kcm/package/contents/ui/DeviceListItem.qml:111 qml/listitemmenu.cpp:387
#, kde-format
msgctxt "Port is unavailable"
msgid "%1 (unavailable)"
msgstr "%1 (nedostupné)"

#: kcm/package/contents/ui/DeviceListItem.qml:113 qml/listitemmenu.cpp:389
#, kde-format
msgctxt "Port is unplugged"
msgid "%1 (unplugged)"
msgstr "%1 (nezapojené)"

#: kcm/package/contents/ui/DeviceListItem.qml:180
#, kde-format
msgctxt "Placeholder is channel name"
msgid "%1:"
msgstr "%1:"

#: kcm/package/contents/ui/DeviceListItem.qml:213
#, kde-format
msgctxt "Perform an audio test of the device"
msgid "Test"
msgstr "Vyskúšať"

#: kcm/package/contents/ui/DeviceListItem.qml:222
#, kde-format
msgctxt "Audio balance (e.g. control left/right volume individually"
msgid "Balance"
msgstr "Vyváženie"

#: kcm/package/contents/ui/main.qml:24
#, kde-format
msgid "This module allows configuring the Pulseaudio sound subsystem."
msgstr "Tento modul umožní nastaviť zvukový subsystém Pulseaudio."

#: kcm/package/contents/ui/main.qml:83
#, kde-format
msgid "Show Inactive Devices"
msgstr "Zobraziť neaktívne zariadenia"

#: kcm/package/contents/ui/main.qml:92
#, fuzzy, kde-format
#| msgid "Configure…"
msgid "Configure Volume Controls…"
msgstr "Nastaviť..."

#: kcm/package/contents/ui/main.qml:99
#, kde-format
msgid "Configure…"
msgstr "Nastaviť..."

#: kcm/package/contents/ui/main.qml:102
#, kde-format
msgid "Requires %1 PulseAudio module"
msgstr "Vyžaduje PulseAudio modul %1"

#: kcm/package/contents/ui/main.qml:105
#, kde-format
msgid ""
"Add virtual output device for simultaneous output on all local sound cards"
msgstr ""
"Pridať virtuálne výstupné zariadenie pre súčasný výstup na všetkých "
"miestnych zvukových kartách"

#: kcm/package/contents/ui/main.qml:111
#, kde-format
msgid ""
"Automatically switch all running streams when a new output becomes available"
msgstr ""
"Automaticky prepnúť všetky bežiace prúdy pri dostupnosti nového výstupu"

#: kcm/package/contents/ui/main.qml:148
#, kde-format
msgid "Playback Devices"
msgstr "Prehrávacie zariadenie"

#: kcm/package/contents/ui/main.qml:172
#, kde-format
msgid "Recording Devices"
msgstr "Nahrávacie zariadenia"

#: kcm/package/contents/ui/main.qml:196
#, kde-format
msgid "Inactive Cards"
msgstr "Neaktívne karty"

#: kcm/package/contents/ui/main.qml:230
#, kde-format
msgid "Playback Streams"
msgstr "Prehrávacie prúdy"

#: kcm/package/contents/ui/main.qml:279
#, kde-format
msgid "Recording Streams"
msgstr "Nahrávacie prúdy"

#: kcm/package/contents/ui/main.qml:313
#, kde-format
msgctxt ""
"%1 is an error string produced by an external component, and probably "
"untranslated"
msgid ""
"Error trying to play a test sound. \n"
"The system said: \"%1\""
msgstr ""
"Chyba pri pokuse o prehranie testovacieho zvuku. \n"
"Systém odpovedal: \"%1\""

#: kcm/package/contents/ui/main.qml:348
#, kde-format
msgid "Front Left"
msgstr "Ľavý predný"

#: kcm/package/contents/ui/main.qml:349
#, kde-format
msgid "Front Center"
msgstr "Ľavý stredný"

#: kcm/package/contents/ui/main.qml:350
#, kde-format
msgid "Front Right"
msgstr "Pravý predný"

#: kcm/package/contents/ui/main.qml:351
#, kde-format
msgid "Side Left"
msgstr "Ľavý stredný"

#: kcm/package/contents/ui/main.qml:352
#, kde-format
msgid "Side Right"
msgstr "Pravý stredný"

#: kcm/package/contents/ui/main.qml:353
#, kde-format
msgid "Rear Left"
msgstr "Ľavý predný"

#: kcm/package/contents/ui/main.qml:354
#, kde-format
msgid "Subwoofer"
msgstr "Subwoofer"

#: kcm/package/contents/ui/main.qml:355
#, kde-format
msgid "Rear Right"
msgstr "Pravý predný"

#: kcm/package/contents/ui/main.qml:356
#, kde-format
msgid "Mono"
msgstr "Mono"

#: kcm/package/contents/ui/main.qml:466
#, kde-format
msgid "Click on any speaker to test sound"
msgstr "Kliknutím na reproduktor otestujete zvuk"

#: kcm/package/contents/ui/MuteButton.qml:22
#, kde-format
msgctxt "Unmute audio stream"
msgid "Unmute %1"
msgstr "Zrušiť stlmenie %1"

#: kcm/package/contents/ui/MuteButton.qml:22
#, kde-format
msgctxt "Mute audio stream"
msgid "Mute %1"
msgstr "Stlmiť %1"

#: kcm/package/contents/ui/StreamListItem.qml:57
#, kde-format
msgid "Notification Sounds"
msgstr "Zvuky notifikácií"

#: kcm/package/contents/ui/StreamListItem.qml:63
#, kde-format
msgctxt "label of stream items"
msgid "%1: %2"
msgstr "%1: %2"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:15
#, kde-format
msgid "Volume Controls"
msgstr ""

#: kcm/package/contents/ui/VolumeControlsConfig.qml:31
#, kde-format
msgid "Raise maximum volume"
msgstr ""

#: kcm/package/contents/ui/VolumeControlsConfig.qml:39
#, kde-format
msgid "Volume change step:"
msgstr ""

#: kcm/package/contents/ui/VolumeControlsConfig.qml:63
#, kde-format
msgid "Play audio feedback for changes to:"
msgstr ""

#: kcm/package/contents/ui/VolumeControlsConfig.qml:64
#: kcm/package/contents/ui/VolumeControlsConfig.qml:73
#, kde-format
msgid "Audio volume"
msgstr ""

#: kcm/package/contents/ui/VolumeControlsConfig.qml:72
#, kde-format
msgid "Show visual feedback for changes to:"
msgstr ""

#: kcm/package/contents/ui/VolumeControlsConfig.qml:80
#, fuzzy, kde-format
#| msgid "Microphone Muted"
msgid "Microphone sensitivity"
msgstr "Mikrofón stlmený"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:87
#, fuzzy, kde-format
#| msgid "Mute audio"
msgid "Mute state"
msgstr "Stlmiť zvuk"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:94
#, fuzzy, kde-format
#| msgid "Default Device"
msgid "Default output device"
msgstr "Predvolené zariadenie"

#: kcm/package/contents/ui/VolumeSlider.qml:51
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1%"

#: kcm/package/contents/ui/VolumeSlider.qml:71
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100%"

#: qml/listitemmenu.cpp:342
#, kde-format
msgid "Play all audio via this device"
msgstr "Prehrávať všetok zvuk cez toto zariadenie"

#: qml/listitemmenu.cpp:347
#, kde-format
msgid "Record all audio via this device"
msgstr "Nahrávať všetok zvuk cez toto zariadenie"

#: qml/listitemmenu.cpp:375
#, kde-format
msgctxt ""
"Heading for a list of ports of a device (for example built-in laptop "
"speakers or a plug for headphones)"
msgid "Ports"
msgstr "Porty"

#: qml/listitemmenu.cpp:445
#, kde-format
msgctxt ""
"Heading for a list of device profiles (5.1 surround sound, stereo, speakers "
"only, ...)"
msgid "Profiles"
msgstr "Profily"

#: qml/listitemmenu.cpp:479
#, kde-format
msgctxt ""
"Heading for a list of possible output devices (speakers, headphones, ...) to "
"choose"
msgid "Play audio using"
msgstr "Prehrávať zvuk pomocou"

#: qml/listitemmenu.cpp:481
#, kde-format
msgctxt ""
"Heading for a list of possible input devices (built-in microphone, "
"headset, ...) to choose"
msgid "Record audio using"
msgstr "Nahrávať zvuk pomocou"

#: qml/microphoneindicator.cpp:102
#, kde-format
msgid "Mute"
msgstr "Stlmiť"

#: qml/microphoneindicator.cpp:132 qml/microphoneindicator.cpp:134
#, kde-format
msgid "Microphone"
msgstr "Mikrofón"

#: qml/microphoneindicator.cpp:134
#, kde-format
msgid "Microphone Muted"
msgstr "Mikrofón stlmený"

#: qml/microphoneindicator.cpp:284
#, kde-format
msgctxt "list separator"
msgid ", "
msgstr ", "

#: qml/microphoneindicator.cpp:284
#, kde-format
msgctxt "List of apps is using mic"
msgid "%1 are using the microphone"
msgstr "%1 používajú mikrofón"

#: qml/microphoneindicator.cpp:310
#, kde-format
msgctxt "App %1 is using mic with name %2"
msgid "%1 is using the microphone (%2)"
msgstr "%1 používa mikrofón (%2)"

#: qml/microphoneindicator.cpp:317
#, kde-format
msgctxt "App is using mic"
msgid "%1 is using the microphone"
msgstr "%1 používa mikrofón"

#~ msgid "100%"
#~ msgstr "100%"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Roman Paholík"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "wizzardsk@gmail.com"

#~ msgctxt "@title"
#~ msgid "Audio"
#~ msgstr "Zvuk"

#~ msgctxt "@info:credit"
#~ msgid "Copyright 2015 Harald Sitter"
#~ msgstr "Copyright 2015 Harald Sitter"

#~ msgctxt "@info:credit"
#~ msgid "Harald Sitter"
#~ msgstr "Harald Sitter"

#~ msgctxt "@info:credit"
#~ msgid "Author"
#~ msgstr "Autor"

#~ msgid "Configure"
#~ msgstr "Nastaviť"

#~ msgid "Device Profiles"
#~ msgstr "Profily zariadenia"

#~ msgid "Advanced Output Configuration"
#~ msgstr "Pokročilé nastavenie výstupu"

#~ msgid "Speaker Placement and Testing"
#~ msgstr "Umiestnenie reproduktorov a Testovanie"

#~ msgctxt "@label"
#~ msgid "Output:"
#~ msgstr "Výstupy:"

#~ msgctxt "Port is unavailable"
#~ msgid " (unavailable)"
#~ msgstr " (nedostupné)"

#~ msgctxt "Port is unplugged"
#~ msgid " (unplugged)"
#~ msgstr " (nezapojené)"

#~ msgid "Configure..."
#~ msgstr "Nastaviť..."

#~ msgctxt "@label"
#~ msgid "No Device Profiles Available"
#~ msgstr "Nie sú dostupné žiadne profily zariadenia"

#~ msgctxt "@label"
#~ msgid "No Playback Devices Available"
#~ msgstr "Nie sú dostupné prehrávacie zariadenia"

#~ msgctxt "@label"
#~ msgid "No Recording Devices Available"
#~ msgstr "Nie sú dostupné nahrávacie zariadenia"

#~ msgctxt "@label"
#~ msgid "No Applications Playing Audio"
#~ msgstr "Žiadne aplikácie neprehrávajú zvuk"

#~ msgctxt "@label"
#~ msgid "No Applications Recording Audio"
#~ msgstr "Žiadne aplikácie nenahrávajú zvuk"

#~ msgctxt "@title:tab"
#~ msgid "Devices"
#~ msgstr "Zariadenia"

#~ msgctxt "@title:tab"
#~ msgid "Applications"
#~ msgstr "Aplikácie"

#~ msgctxt "@title:tab"
#~ msgid "Advanced"
#~ msgstr "Pokročilé"

#~ msgid "Outputs"
#~ msgstr "Výstupy"

#~ msgctxt "@label"
#~ msgid "No Output Devices Available"
#~ msgstr "Nie sú dostupné výstupné zariadenia"

#~ msgid "Inputs"
#~ msgstr "Vstupy"

#~ msgctxt "@title"
#~ msgid "Configure the Audio Volume"
#~ msgstr "Nastaviť hlasitosť zvuku"

#~ msgid "Capture"
#~ msgstr "Zachytiť"

#~ msgctxt "@label"
#~ msgid "No Additional Configuration Available"
#~ msgstr "Nie je dostupné ďalšie nastavenie"
